/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package problemsolving3;

/**
 *
 * @author Ariful
 */

public class TuitionPaymentTest
    {
       public static void main( String[] args )
      {
         // create subclass objects                                          
         WeeklyPayemnt weeklyPayment =                                 
         new WeeklyPayemnt(  "Saiful",  "Kaif" ,  "111-11-1111", 800.00 );
         HourlyPayment hourlypayment =                                     
         new HourlyPayment( "Karen", "Price" ,  "222-22-2222", 16.75, 40 );
         TutorComission tutorComission =                             
        new TutorComission(                                          
         "Sue",  "Jones", "333-33-3333", 10000, .06 );
         BasePaymentTutor basePaymentTutor =             
         new BasePaymentTutor(                                  
            "Bob", "Lewis", "444-44-4444", 5000, .04, 300 ); 
                         

         System.out.println( "Tutor processed individually:\n" );

         
         
          System.out.printf( "%s\n%s: $%,.2f\n\n",
            weeklyPayment, "earned", weeklyPayment.earnings() );
         System.out.printf( "%s\n%s: $%,.2f\n\n",
            hourlypayment, "earned", hourlypayment.earnings() );
         System.out.printf( "%s\n%s: $%,.2f\n\n",
            tutorComission, "earned", tutorComission.earnings() );

         
          Tutor[] tutors = new Tutor[ 4 ];

        // initialize array with Employees
         tutors[ 0 ] = weeklyPayment;          
         tutors[ 1 ] = hourlypayment;            
         tutors[ 2 ] = tutorComission; 
         tutors[ 3 ] = basePaymentTutor;
      
     

         System.out.println( "Employees processed polymorphically:\n" );

         // generically process each element in array employees
         for ( Tutor currentEmployee : tutors )
         {
            System.out.println( currentEmployee ); // invokes toString

            // determine whether element is a BasePlusCommissionEmployee
           if ( currentEmployee instanceof BasePaymentTutor )
           {
               // downcast Employee reference to
               // BasePlusCommissionEmployee reference
               BasePaymentTutor tutor =
                  ( BasePaymentTutor ) currentEmployee;

               tutor.setBaseSalary( 1.10 * tutor.getBaseSalary() );

               System.out.printf(
                  "new base salary with 10%% increase is: $%,.2f\n",
                  tutor.getBaseSalary() );
            }

            System.out.printf(
               "earned $%,.2f\n\n", currentEmployee.earnings() );
         } // end for

         // get type name of each object in employees array
         for ( int j = 0; j < tutors.length; j++ )      
            System.out.printf(  "Employee %d is a %s\n", j,
               tutors[ j ].getClass().getName() );      
      } // end main
   } 
