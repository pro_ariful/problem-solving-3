/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package student;

/**
 *
 * @author Md Ariful Islam, Hekam
 */
class Question2{
   
   public void show()
   {
	System.out.println("show() method of parent class");
   }	   
}
public class Student extends Question2{
 
   public void disp(){
	System.out.println("show() method of Child class");
   }
   public void newMethod(){
	System.out.println("new method of child class");
   }
   public static void main( String args[]) {
	
	Question2 obj = new Question2();
	obj.show();

	Question2 obj2 = new Student();
	obj2.show();
   }
}
