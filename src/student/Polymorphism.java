/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package student;

/**
 *
 * @author Ariful, hekam
 */
class Overloaded
{
    void example (int x)
    {
       System.out.println ("x: " + x);
    }
    void example (int x, int y)
    {
       System.out.println ("x and y: " + x + "," + y);
    }
    double example(double x) {
       System.out.println("double x: " + x);
       return x*x;
    }
}
public class Polymorphism
{
    public static void main (String args [])
    {
        Overloaded Obj = new Overloaded();
        double result;
        Obj .example(5);
        Obj .example(5, 10);
        result = Obj .example(5.5);
        System.out.println("O/P : " + result);
    }
}

